using System;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using testAssignment.Models;

namespace testAssignment.Controllers
{
    [ApiController]
    [Route("api")]
    [Produces("application/json")]
    public class TestController : ControllerBase
    {
        [HttpGet("reverse")]
        public IActionResult Reverse([FromQuery] string data)
        {
            var result = int.TryParse(data, out var numericValue)
                ? Math.Sqrt(numericValue).ToString(CultureInfo.CurrentCulture)
                : ReverseString(data);

            return Ok(new Response {Result = result});
        }

        private static string ReverseString(string str)
        {
            var charArr = str.ToCharArray();
            Array.Reverse(charArr);
            return new string(charArr);
        }
    }
}